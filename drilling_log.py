import matplotlib.pyplot as plt
import matplotlib.transforms
import lasio
import json
import collections
import pandas as pd
from matplotlib.widgets import Slider, Button , RadioButtons
import numpy as np

#Import the json data structure containing the drilling 
#parameters and their default values

#plt.figure(figsize=(18,28))
#Imports a json with what should be a user given configuration
class import_config():

    def __init__(self,fname):
        self.fname = fname
        with open(self.fname,encoding='utf-8') as f:
            self.parameters = json.load(f)

    def get_val(self,param):
        return self.parameters[param]

    def get_all_vals(self):
        return self.parameters
#config = import_config('parameters.json')
mconfig = import_config('micropal.json')
topsconfig = import_config('formation_top.json')

#gridsize = [3,3]
#Create grid as per the above size row then column
#Rowsize adjusted when class initiated
class Grid():

    def __init__(self,gridsize,rowsize,figsize,grid=True):
        global fig

        fig, ax = plt.subplots(figsize=figsize)
# Needed for printing the file to an svg for example
        self.ax_list = []
        plt.style.use('ggplot')
        plt.subplots_adjust(top=0.85)
        self.gridsize=gridsize
        self.rowsize = rowsize
        #print(plt.style.available)


        if grid == True:
            self.make_grid()
        else:
            self.make_4_1_grid()
    def make_grid(self):
        for i in range(self.gridsize[0]):
            self.ax_list.append(plt.subplot2grid(self.gridsize,(0,i),rowspan=self.rowsize))

    def make_4_1_grid(self):
        ax1 = plt.subplot2grid((8,8),(0,0),rowspan=5,colspan=2)
        ax2 = plt.subplot2grid((8,8),(0,2),rowspan=5,colspan=2)
        ax3 = plt.subplot2grid((8,8),(0,4),rowspan=5,colspan=2)
        ax4 = plt.subplot2grid((8,8),(0,6),rowspan=5,colspan=2)
        ax5 = plt.subplot2grid((8,8),(6,0),colspan=8,rowspan=2)
        self.ax_list.append(ax1)
        self.ax_list.append(ax2)
        self.ax_list.append(ax3)
        self.ax_list.append(ax4)
        self.ax_list.append(ax5)


    def get_ax_objects(self):
        return self.ax_list

#Class for the main plot emulating the usual rigscreen
class DrillingGraph():

    def __init__(self,fname,ax_object,main_plot='GRCX',depth='DEPTH',config='parameters.json',excel=False):
       self.name = fname
       self.twinaxis = collections.OrderedDict()
       self.depth = depth
       if excel == False:
           self.data = lasio.read(fname)
           self.curves = self.data.curves
       else:

           try:
               excel_log = ReadExcelLog(excel)
               self.data = excel_log.data

           except:
               print("Reading the excel file went wrong")

       self.ax = ax_object
       self.main_plot = main_plot
       self.twinaxis[main_plot] = self.ax
       self.config = import_config(config)
#       self.trans = matplotlib.transforms.blended_transform_factory(self.ax.transAxes,self.ax.transData)
       #self.ax2 = self.ax.twinx()

    def get_graph(self):
        return self.ax

    def get_name(self):
        return self.name

    def plot_main(self):

       self.ax.plot(self.data[self.main_plot],self.data[self.depth],color='Red')
       self.ax.invert_yaxis()
       self.ax.xaxis.set_ticks_position('bottom')
       self.ax.tick_params(axis='x',colors='red')
       self.ax.spines['bottom'].set_position(('outward',10))
       self.ax.spines['bottom'].set_color('Red')

       self.ax.set_xlabel('Gamma ray (API)',color='Red',fontsize=8)
       self.ax.xaxis.labelpad = 10

       self.set_ylim()

    def gen_plot(self,name):
        plt.title(self.name,y=1.20)

    def twin_ax(self,param):
        twin_param = self.ax.twiny()
        twin_param.grid(None)
        self.twinaxis[param] = twin_param
        try:
            twin_param.plot(self.data[param],self.data[self.depth],
                    color=self.config.get_val(param)[0])
        except:
            print("Your parameter did not work")

        #twin_param.set_xlabel(param,color=config.get_val(param)[0])
#
    def adjust_spines(self):
        for i,axis in enumerate(self.twinaxis.values()):
            #to skip adjusting the GR we skip the first index
            if i == 0:
                pass
            else:
                axis.spines['top'].set_position(('axes',0.975+i/20))

        for name,axis in self.twinaxis.items():
            if name == 'GRCX':
                pass
            else:
                axis.spines['top'].set_color(self.config.get_val(name)[0])
                axis.tick_params(axis='x',labelcolor=self.config.get_val(name)[0])
                axis.set_xlabel(self.config.get_val(name)[2],color=self.config.get_val(name)[0],fontsize=8)
                axis.xaxis.set_tick_params(labeltop='on')

    def adjust_spine(self,axis,pos,loc='top'):
       axis.spines[loc].set_position(('axes',pos))

    def set_xlimits(self):
        for name,axis in self.twinaxis.items():
            axis.set_xlim(self.config.get_val(name)[1])

    def set_ylim(self,limits=None):
        try:
            if limits == None:
                self.ax.set_ylim((max(self.data['DEPTH']),min(self.data['DEPTH'])))
            else:
                self.ax.set_ylim(max(limits),min(limits))
        except:
            print("unable to automatically set scale")

    def set_yticks(self):
        ticks = np.arange(min(self.data['DEPTH']),max(self.data['DEPTH']),100)
        self.ax.set_yticks(ticks)

    def final_plot(self):
        plt.show()


#Read micropal sheet from excel sheet

class ReadMicro():

    def __init__(self,fname):
        self.scbs = fname
        self.micropal = self.micropal_read()

        #setup so that sample column = the index
        self.micropal['Sample depth (MD)'] = self.micropal.index

    def micropal_read(self):
        micropal_sheet = pd.read_excel(self.scbs,
                                        sheetname='Micropal',header=0,
                                        index_col='Sample depth (MD)')
        return(micropal_sheet)

    def cleanupdata(self,col,oldval,newval):
        try:
            self.micropal[col] = self.micropal[col].replace({oldval,newval})
        except:
            print("Cleaning sheet did not work,likely text values in the numeric data")

    def convert_to_numbers(self):
        self.micropal[self.micropal.columns[18:30]] = micropal[micropal.columns[18:30]].convert_objects(convert_numeric=True)

    def get_micropal(self):
        return self.micropal

#class to read GR data from excel sheet

class ReadExcelLog():
    '''Reads excel logs with a simple header'''

    def __init__(self, fname):
        self.gr_file = fname
        self.data = self.gr_read()

    def gr_read(self):
        gr_data = pd.read_excel(self.gr_file,header=1)

        return(gr_data)



#Lazy way of doing lithology colours until can figure out how to use the json 
lith_colors = ['lawngreen','lightgreen','darkgrey','lightgrey',
                'lightsteelblue','lavender','skyblue',
                'dodgerblue','c','mediumblue','midnightblue','darkblue']
class MicroGraph():

    def __init__(self,micropal,ax_object,microconfig,leg=False):
        self.micropal = ReadMicro(micropal).get_micropal()

        self.micropal[self.micropal.columns[18:30]].plot.barh(stacked=True,
                    ax=ax_object,color=lith_colors,width=1,edgecolor='black',linewidth=0.8).legend(
                    loc='upper left', bbox_to_anchor=(0.25,1.10))
        if leg == True:
            ax_object.legend().draggable()
        ax_object.invert_yaxis()
        ax_object.set_title("Lithological Log",y=1.12)
        ax_object.set_xlim(0,100)

#Scale looked quite bad and porosity doesnt match with depth 

        #ax_object2 = ax_object.twinx()
        #ax_object2.plot(self.micropal['Porosity Percentage %']*100,
        #                self.micropal['Sample depth (MD)'],color='blue')
        #ax_object.invert_yaxis()
        #ax_object2.invert_yaxis()



#test = grid(gridsize,3)

#More specific layouts
#Drilling graph with slider at the bottom
class NormalDrillingGraph(DrillingGraph):
    def __init__(self,fname,ax):
        super().__init__(fname,ax)
        self.plot_main()
        self.twin_ax('ROP_AVG')
        self.twin_ax('WOBCX')
        self.twin_ax('INCB')
        self.adjust_spines()
        self.set_xlimits()

#Interactive slider element - doesnt work too well

#        y_ranges = plt.axes([0.40,0.01,0.23,0.03],axisbg ='blue')
#        self.yslider = Slider(y_ranges,'YRanges',0,3,valinit=0)
#        self.yslider.on_changed(self.update)

        #bnext = figure[0.7,0.05,0.1,0.075]

#        self.button = Button(plt.axes[0.40,0.01,0.23,0.03],'Next')
#        self.button.on_clicked(self.next)
#        self.ind = 0

    def next(self,event):
       self.ind += 1
       print(self.ind)

    def update(self,val):
        slider_val  = self.yslider.val
        self.ax.lines[0].set_xdata(self.data['GRCX']*slider_val)
        fig.canvas.draw_idle()


#GR and Inclination
class GRINCGraph(DrillingGraph):
    def __init__(self,fname,ax):
        super().__init__(fname,ax)
        self.plot_main()
        self.twin_ax('INCB')
        self.twin_ax('ROP_AVG')
        self.adjust_spines()
        self.set_xlimits()
        ax.set_title("GR / Inclination Plot",y=1.12)
        self.adjust_spine(self.ax,pos=.12)

#Porosity in samples and Annular pressure
class PorAnnularGraph(DrillingGraph):

    def __init__(self,fname,ax,micropal,mconfig):
        super().__init__(fname,ax)

        #Reading micropal data takes up quite some time (due to size of excel file?)
        #Should maybe find a way to inherit the micropal class?
        #Though maybe not that useful since we can get the data with just one line
        self.micropal = ReadMicro(micropal).get_micropal()
        self.mconfig = mconfig

        #self.data = lasio.read(fname)
        self.curves = self.data.curves
        ax.plot(self.micropal['Porosity Percentage %'],
                self.micropal['Sample depth (MD)'],color='blue')
        ax.invert_yaxis()
        self.twin_ax('APRESX')
        self.twinaxis["APRESX"].set_xlim(2000,6000)
   
        ax.set_title("Porosity / Bottomhole Pressure",y=1.12)
        ax.set_xlim(0,5)

#needs to be in config json
image_params = {"Horizontal Offset":1,"Vertical Offset":55,
                     "Grid":5,"Spacing":0.1,"Dirname":"D:/bio/L1/Imag*.jpg"}

#works for adding an image log in a grid
import glob
import re
import matplotlib.image as image
class ImageGraph():

    def __init__(self,ax,params,imagedirectory=None,ylim=(12800,13200)):
       if imagedirectory == None:
           self.filepaths = glob.glob(params["Dirname"])
       else:
           self.filepaths = glob.glob(imagedirectory)
       self.ax = ax
       self.filepaths.sort()
       self.ho = params["Horizontal Offset"]
       self.vo = params["Vertical Offset"]
       self.grid = params["Grid"]
       self.spacing = params["Spacing"]
       self.process_images()
       self.maxi = max(ylim)
       self.minim = min(ylim)

       ax.set_ylim(self.maxi,self.minim)
       ax.set_xlim(-1.5,self.grid-0.5)
       #ax.axis('off')
       ax.set_axisbelow(True)
       ax.get_xaxis().set_visible(False)
       ax.grid(False)
       ax.set_title("Sample Images",y=1.12)


    def process_images(self):
        for index,filepath in enumerate(self.filepaths):
            print(index,filepath)
            im = image.imread(filepath)
            depth = re.findall(r'[\d+]{5}',filepath)
            depth = int(depth[0])
            if index == 0 :
                self.minim = depth
            elif index == len(self.filepaths)-1:
                self.maxi = depth
            if depth:
                if (index%self.grid) < ((index-1)%self.grid):
                    self.ax.imshow(im,aspect='auto',extent=(((index%self.grid)-self.ho),
                                index%self.grid,depth,depth-self.vo),zorder=-1)
                else:
                    self.ax.imshow(im,aspect='auto', extent=(((index-self.ho)%self.grid),
                                index%self.grid,depth,depth-self.vo),zorder=-1)

class Annotate():
    def __init__(self,dicto,ax,colordict,data):
        self.ax = ax
        self.dicto = dicto
        self.color_dict = colordict.get_all_vals()
        try:
            self.data = lasio.read(data)
        except:
            print("This is not a las file, unable to retrieve TVD")

    def make_line(self,line_range=[0,100],offset=50,restrict=True):
        plot_range = self.ax.get_ylim()
        for key,depths_list in self.dicto.items():
            for depth in depths_list:
                if (depth < plot_range[1] or depth > plot_range[0]) and restrict == True:
                    pass
                elif key in self.color_dict:
                    try:
                        print("Plotting TVD for " + key)
                        tvd = str(self.get_tvd(depth))+"'TVD"
                        self.ax.plot(line_range,[depth,depth],color=self.color_dict[key],linestyle='dashed',zorder=20)
                        self.ax.text(offset,depth,key+ ' '+tvd,style='italic',size=7,
                            bbox={'facecolor':self.color_dict[key],'alpha':0.5,'pad':10},zorder=20)
                    except:
                        print("TVD did not work because likely not in range")
                        self.ax.plot(line_range,[depth,depth],color=self.color_dict[key],linestyle='dashed',zorder=20)
                        self.ax.text(offset,depth,key,style='italic',size=7,
                            bbox={'facecolor':self.color_dict[key],'alpha':0.5,'pad':10},zorder=20)
                elif key not in self.color_dict:
                    self.ax.plot(line_range,[depth,depth],color='black',linestyle='dashed',zorder=20)
                    self.ax.text(offset,depth,key,size=7,color='white',
                            bbox={'facecolor':'black','alpha':0.5,'pad':10},zorder=20)

    def get_tvd(self,md):
       return int(self.data['TVD'][self.data['DEPTH']==md])
    def make_arrow(self,depth,annotation):
        pass

#create a stats table with blank spaces
from matplotlib.table import Table
class StatsTable():
    def __init__(self,ax_object):
        ax_object.table(cellText=[[''],[''],[''],[''],['']],
                        colWidths=[0.35,0.25],
                        rowLabels=[u"ST Point (Depth in 'MD/TVD/FM)",u"Total Footage 'MD",
                                    u"Footage From TE 'MD",u"Reservoir Contact 'MD",
                                    u"Planned Footage 'MD"],
                        loc='top',bbox=[0.35,0.9,0.4,0.1])
        ax_object.axis('off')


#SSL Still needs some work
import time
from PIL import Image
from matplotlib.ticker import MultipleLocator
class SSL():
    def __init__(self,lasfname,micropal,mconfig,savename,anno_dict,imagedirectory,ylims=None,print_=False):
        self.grid = Grid((7,7),10,figsize=(80,42))
        if ylims == None: 
            GRINCGraph(lasfname,self.grid.get_ax_objects()[1])
            PorAnnularGraph(lasfname,self.grid.get_ax_objects()[2],micropal,mconfig)
        else:
            GRINCGraph(lasfname,self.grid.get_ax_objects()[1]).set_ylim(ylims)
            PorAnnularGraph(lasfname,self.grid.get_ax_objects()[2],micropal,mconfig).set_ylim(ylims)

        Annotate(anno_dict,self.grid.get_ax_objects()[1],topsconfig,lasfname).make_line()
        MicroGraph(micropal,self.grid.get_ax_objects()[3],mconfig)
        ImageGraph(self.grid.get_ax_objects()[4],image_params,imagedirectory,ylims)
        self.grid.get_ax_objects()[5].axis('off')
        self.grid.get_ax_objects()[6].axis('off')
        self.grid.get_ax_objects()[5].set_title("Operation",y=1.12)
        self.grid.get_ax_objects()[6].set_title('Interpretation',y=1.12)
        plt.suptitle("Stratigraphic Summary Log \n"+time.strftime("%d/%m/%Y"),fontsize=25)
        millennia_logo = Image.open('Millennia.png')
        fig.figimage(millennia_logo,0,3920)
        client_logo = Image.open('logo.jpg')
        fig.figimage(client_logo,420,4035)
        fig.text(0.0015,0.90,'Biostratigraphers:',fontsize=15)
        fig.text(0.068,0.70,'Lateral summary:',fontsize=25)


        #Formatter for the yaxis
        majorLocator = MultipleLocator(20)
        majorLocator1 = MultipleLocator(20)
        majorLocator2 = MultipleLocator(20)
        #Set grid lines every 20 'MD
        self.grid.ax_list[1].yaxis.set_major_locator(majorLocator)
        self.grid.ax_list[2].yaxis.set_major_locator(majorLocator1)
        self.grid.ax_list[4].yaxis.set_major_locator(majorLocator2)
        #Show them, there seems to be a bug where they dont show automatically
        for i in self.grid.ax_list[1].axes.get_yticklabels():
            i.set_visible(True)
        for i in self.grid.ax_list[2].axes.get_yticklabels():
            i.set_visible(True)
        for i in self.grid.ax_list[4].axes.get_yticklabels():
            i.set_visible(True)
        if print_ == True:
            StatsTable(self.grid.get_ax_objects()[0])
            plt.savefig(savename)

#Reasonable template of what can be made daily
class DailyLog():
    def __init__(self,lasname,micropal,mconfig,savename):
        self.grid = Grid((3,3),3,figsize=(40,27))
        GRINCGraph(lasname,self.grid.get_ax_objects()[0])
        MicroGraph(micropal,self.grid.get_ax_objects()[1],micropal,leg=True)
        PorAnnularGraph(lasname,self.grid.get_ax_objects()[2],micropal,mconfig)
        plt.suptitle('Correlation '+ time.strftime("%d/%m/%Y"),fontsize=20,y=0.99)
        plt.savefig(savename)



#Possible templates below



class Normal_log():
    def __init__(self,fname,ftype='pdf'):
        self.config = import_config('parameters.json')
        self.grid = Grid((1,1),1,figsize=(10,20))
        ng = NormalDrillingGraph(fname ,self.grid.get_ax_objects()[0])
        plt.savefig(fname[:-3]+ftype)


#Setup for the axis objects to plot



#Maximize the window
#Remove the two lines below when you want to print

#This works for maximizing with Tkinter

#mng  = plt.get_current_fig_manager()
#mng.resize(*mng.window.maxsize())

#Works for Qt
#figManger = plt.get_current_fig_manager()
#figManger.window.showMaximized()
##
#plt.show()
