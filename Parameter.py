# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Parameter_Editor.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(570, 246)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget_3 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(10, 20, 101, 159))
        self.verticalLayoutWidget_3.setObjectName("verticalLayoutWidget_3")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.ROP_CBox = QtWidgets.QCheckBox(self.verticalLayoutWidget_3)
        self.ROP_CBox.setObjectName("ROP_CBox")
        self.verticalLayout_5.addWidget(self.ROP_CBox)
        self.label_7 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_5.addWidget(self.label_7)
        self.lineEdit_7 = QtWidgets.QLineEdit(self.verticalLayoutWidget_3)
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.verticalLayout_5.addWidget(self.lineEdit_7)
        self.label_8 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_8.setObjectName("label_8")
        self.verticalLayout_5.addWidget(self.label_8)
        self.lineEdit_8 = QtWidgets.QLineEdit(self.verticalLayoutWidget_3)
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.verticalLayout_5.addWidget(self.lineEdit_8)
        self.verticalLayoutWidget_4 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(120, 20, 101, 159))
        self.verticalLayoutWidget_4.setObjectName("verticalLayoutWidget_4")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.WOB_CBox = QtWidgets.QCheckBox(self.verticalLayoutWidget_4)
        self.WOB_CBox.setObjectName("WOB_CBox")
        self.verticalLayout_6.addWidget(self.WOB_CBox)
        self.label_9 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        self.label_9.setObjectName("label_9")
        self.verticalLayout_6.addWidget(self.label_9)
        self.lineEdit_9 = QtWidgets.QLineEdit(self.verticalLayoutWidget_4)
        self.lineEdit_9.setObjectName("lineEdit_9")
        self.verticalLayout_6.addWidget(self.lineEdit_9)
        self.label_10 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        self.label_10.setObjectName("label_10")
        self.verticalLayout_6.addWidget(self.label_10)
        self.lineEdit_10 = QtWidgets.QLineEdit(self.verticalLayoutWidget_4)
        self.lineEdit_10.setObjectName("lineEdit_10")
        self.verticalLayout_6.addWidget(self.lineEdit_10)
        self.verticalLayoutWidget_5 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(230, 20, 101, 159))
        self.verticalLayoutWidget_5.setObjectName("verticalLayoutWidget_5")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.GR_CBox = QtWidgets.QCheckBox(self.verticalLayoutWidget_5)
        self.GR_CBox.setObjectName("GR_CBox")
        self.verticalLayout_7.addWidget(self.GR_CBox)
        self.label_11 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_11.setObjectName("label_11")
        self.verticalLayout_7.addWidget(self.label_11)
        self.lineEdit_11 = QtWidgets.QLineEdit(self.verticalLayoutWidget_5)
        self.lineEdit_11.setObjectName("lineEdit_11")
        self.verticalLayout_7.addWidget(self.lineEdit_11)
        self.label_12 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_12.setObjectName("label_12")
        self.verticalLayout_7.addWidget(self.label_12)
        self.lineEdit_12 = QtWidgets.QLineEdit(self.verticalLayoutWidget_5)
        self.lineEdit_12.setObjectName("lineEdit_12")
        self.verticalLayout_7.addWidget(self.lineEdit_12)
        self.verticalLayoutWidget_6 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_6.setGeometry(QtCore.QRect(340, 20, 101, 159))
        self.verticalLayoutWidget_6.setObjectName("verticalLayoutWidget_6")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_6)
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.NBI_CBox = QtWidgets.QCheckBox(self.verticalLayoutWidget_6)
        self.NBI_CBox.setObjectName("NBI_CBox")
        self.verticalLayout_8.addWidget(self.NBI_CBox)
        self.label_13 = QtWidgets.QLabel(self.verticalLayoutWidget_6)
        self.label_13.setObjectName("label_13")
        self.verticalLayout_8.addWidget(self.label_13)
        self.lineEdit_13 = QtWidgets.QLineEdit(self.verticalLayoutWidget_6)
        self.lineEdit_13.setObjectName("lineEdit_13")
        self.verticalLayout_8.addWidget(self.lineEdit_13)
        self.label_14 = QtWidgets.QLabel(self.verticalLayoutWidget_6)
        self.label_14.setObjectName("label_14")
        self.verticalLayout_8.addWidget(self.label_14)
        self.lineEdit_14 = QtWidgets.QLineEdit(self.verticalLayoutWidget_6)
        self.lineEdit_14.setObjectName("lineEdit_14")
        self.verticalLayout_8.addWidget(self.lineEdit_14)
        self.verticalLayoutWidget_7 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_7.setGeometry(QtCore.QRect(450, 20, 101, 159))
        self.verticalLayoutWidget_7.setObjectName("verticalLayoutWidget_7")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_7)
        self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.APREX_CBox = QtWidgets.QCheckBox(self.verticalLayoutWidget_7)
        self.APREX_CBox.setObjectName("APREX_CBox")
        self.verticalLayout_9.addWidget(self.APREX_CBox)
        self.label_15 = QtWidgets.QLabel(self.verticalLayoutWidget_7)
        self.label_15.setObjectName("label_15")
        self.verticalLayout_9.addWidget(self.label_15)
        self.lineEdit_15 = QtWidgets.QLineEdit(self.verticalLayoutWidget_7)
        self.lineEdit_15.setObjectName("lineEdit_15")
#        #Set text changed
#        self.lineEdit_15.setText("Some Number")
#        print(self.lineEdit_15.text())
        self.verticalLayout_9.addWidget(self.lineEdit_15)
        self.label_16 = QtWidgets.QLabel(self.verticalLayoutWidget_7)
        self.label_16.setObjectName("label_16")
        self.verticalLayout_9.addWidget(self.label_16)
        self.lineEdit_16 = QtWidgets.QLineEdit(self.verticalLayoutWidget_7)
        self.lineEdit_16.setObjectName("lineEdit_16")
        self.verticalLayout_9.addWidget(self.lineEdit_16)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(439, 200, 111, 31))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(20, 200, 151, 32))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(190, 200, 121, 32))
        self.pushButton_3.setObjectName("pushButton_3")
        self.verticalLayoutWidget_3.raise_()
        self.verticalLayoutWidget_4.raise_()
        self.verticalLayoutWidget_5.raise_()
        self.verticalLayoutWidget_6.raise_()
        self.verticalLayoutWidget_7.raise_()
        self.pushButton.raise_()
        self.pushButton_2.raise_()
        self.pushButton_3.raise_()
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.ROP_CBox.setText(_translate("MainWindow", "ROP"))
        self.label_7.setText(_translate("MainWindow", "Minimum"))
        self.label_8.setText(_translate("MainWindow", "Maximum"))
        self.WOB_CBox.setText(_translate("MainWindow", "WOB"))
        self.label_9.setText(_translate("MainWindow", "Minimum"))
        self.label_10.setText(_translate("MainWindow", "Maximum"))
        self.GR_CBox.setText(_translate("MainWindow", "GR"))
        self.label_11.setText(_translate("MainWindow", "Minimum"))
        self.label_12.setText(_translate("MainWindow", "Maximum"))
        self.NBI_CBox.setText(_translate("MainWindow", "NBI"))
        self.label_13.setText(_translate("MainWindow", "Minimum"))
        self.label_14.setText(_translate("MainWindow", "Maximum"))
        self.APREX_CBox.setText(_translate("MainWindow", "APREX"))
        self.label_15.setText(_translate("MainWindow", "Minimum"))
        self.label_16.setText(_translate("MainWindow", "Maximum"))
        self.pushButton.setText(_translate("MainWindow", "Save_Changes"))
        self.pushButton_2.setText(_translate("MainWindow", "Generate_Las_PDF"))
        self.pushButton_3.setText(_translate("MainWindow", "Generate_SVG"))


        self.pushButton.clicked.connect(self.saveChanges)
        self.pushButton_2.clicked.connect(self.genPDF)
        self.pushButton_3.clicked.connect(self.genSVG)
        self.setTexts()

    def setTexts(self):
        import json
        self.fname = 'parameters.json'
        with open(self.fname,encoding='utf-8') as f:
            self.parameters = json.load(f)
        #print(self.parameters)
        self.lineEdit_7.setText(str(self.parameters['ROP_AVG'][1][0]))
        self.lineEdit_8.setText(str(self.parameters['ROP_AVG'][1][1]))

        self.lineEdit_9.setText(str(self.parameters['WOBCX'][1][0]))
        self.lineEdit_10.setText(str(self.parameters['WOBCX'][1][1]))

        self.lineEdit_11.setText(str(self.parameters['GRCX'][1][0]))
        self.lineEdit_12.setText(str(self.parameters['GRCX'][1][1]))

        self.lineEdit_13.setText(str(self.parameters['INCB'][1][0]))
        self.lineEdit_14.setText(str(self.parameters['INCB'][1][1]))

        self.lineEdit_15.setText(str(self.parameters['APRESX'][1][0]))
        self.lineEdit_16.setText(str(self.parameters['APRESX'][1][1]))
    def saveChanges(self):
        import json
        #print(self.lineEdit_15.text())
        with open(self.fname,'r+',encoding='utf-8') as f:
            data = json.load(f)
            data['ROP_AVG'][1][0] = int(self.lineEdit_7.text())
            data['ROP_AVG'][1][1] = int(self.lineEdit_8.text())

            data['WOBCX'][1][0] = int(self.lineEdit_9.text())
            data['WOBCX'][1][1] = int(self.lineEdit_10.text())

            data['GRCX'][1][0] = int(self.lineEdit_11.text())
            data['GRCX'][1][1] = int(self.lineEdit_12.text())

            data['INCB'][1][0] = int(self.lineEdit_13.text())
            data['INCB'][1][1] = int(self.lineEdit_14.text())

            data['APRESX'][1][0] = int(self.lineEdit_15.text())
            data['APRESX'][1][1] = int(self.lineEdit_16.text())

            f.seek(0)
            json.dump(data,f)
            f.truncate()

    def genPDF(self):
        from drilling_log import Normal_log
        fname = self.showDialog()
        fname = fname[0]
        print(fname)
        test = Normal_log(fname)
        print("Changes saved")
    def showDialog(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(None,'/home/ziad/',"las files")
        return fname

    def genSVG(self):
        from drilling_log import Normal_log
        fname = self.showDialog()
        fname = fname[0]
        print(fname)
        test = Normal_log(fname,ftype='svg')


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

