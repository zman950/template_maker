import json

configuration = {'TVD':['Black',(0,12000),'True Vertical Depth (feet)'],
                 'ROP_AVG':['Blue',(0,120),'Rate of Penetration (ft/hr)'],
                 'GRCX':['Red',(0,80),'Gamma Ray (API)'],
                 'WOBCX':['Green',(0,20),'Weight on bit (klbf)'],
                 'INCB':['Black',(70,100),'Inclination (degrees)'],
                 'HKLD_MAX':['Blue',(28000,42000),'String Weight (klbf)'],
                 'APRESX':['Pink',(3000,4000),'Bottomhole Pressure (psi)'],
                 'UBELVINGJFRG':['Orange',(70,100),'Flow Rate (gal/min)'],
                 'AZTB':['Brown',(0,360),'Azimuth (degrees)'],
                 'TFINX':['Brown',(80,100),'Inclination (degrees)']
}

with open('parameters.json','w') as f:
    json.dump(configuration,f)

micropal_config  = {
                 'Porosity %':['Blue',(0,15),'Porosity Percentage %'],
                 'MXF':['Lawngreen','Microcrystalline Fine'],
                 'MXC':['Lightgreen','Microcrystalline Coarse'],
                 'MS':['darkgrey','Mudstone'],
                 'MS-WS ':['lightgrey','Mudstone-Wackestone'],
                 'Bio-WS':['lightsteelblue','Bioclastic Wackestone'],
                 'Pel-WS':['lavender','Peloid Wackestone'],
                 'Bio-PS':['skyblue','Bioclastic Packstone'],
                 'Bio-Pel PS':['dodgerblue','Bioclastic-Peloidal Packstone'],
                 'Pel-Ooid PS':['c','Peloidal-Ooid Packestone'],
                 'Bio-GS':['mediumblue','Bioclastic Grainstone'],
                 'Bio-Pel GS':['midnightblue','Bioclastic-Peloidal Grainstone'],
                 'Pel-Ooid GS':['darkblue','Peloidal-Ooid Grainstone'],
}

with open('micropal.json','w') as f:
    json.dump(micropal_config,f)

formation_tops_config = {
                'KCC':'lightgray','KC01':'darkgray','KC02':'yellow',
                'KC03':'lightskyblue','KC04':'darksalmon','KC05':'lightgreen',
                'KC06':'purple','KC07':'turquoise','KC08':'pink',
                'KB02':'grey','KB03':'grey','KB04':'grey',
                'KB05':'grey','KB06':'grey','KB07':'grey'
}

with open('formation_top.json','w') as f:
    json.dump(formation_tops_config,f)
