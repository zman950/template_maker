#!/usr/bin/python3

import matplotlib
import matplotlib.pyplot as plt 
from matplotlib.backends.backend_pdf import PdfPages 
import numpy as np
import argparse
import csv


#parses the filename to make life easier
#also parses scales for given parameters
parser = argparse.ArgumentParser()
parser.add_argument("--fname", help="file to plot")
parser.add_argument("--anno", help="to plot annotations from arg")
parser.add_argument("--yscale", help="define plotted range")
parser.add_argument("--xscaler", help="sets x scale for given rop")
parser.add_argument("--xscaleg", help="sets x scale for given gr")
parser.add_argument("--xscalea", help="sets x scale for given aprx")
parser.add_argument("--xscalen", help="sets x scale for given nbi")
parser.add_argument("--xscalew", help="sets x scale for given wob")
args = parser.parse_args()

def get_fname():
    global fname
    fname = 'HRDH_937_5_03OCT2015_GR_ML_BHI.las'
    if args.fname:
        fname = str(args.fname)
get_fname()
#Choose style and las

field_names = ['KB05','KB06','KB07','KCC',
                'KC01','KC02','KC03','KC04',
                'KC05','KC06','KC07','KC08',
                'KC09','KC10','KC11','KC12',
                'Top','Bottom','POOH']
tops_list = {}
for name in field_names:
    tops_list[name] = []
print(tops_list.keys())
x = list(zip(*tops_list.values()))
print(x)
#file writers and importer for current data structure

def writer(file_name,tops_list,field_name):
    fname = file_name
    print(fname)
    #to be modified with more params 
    keys = sorted(list(tops_list.keys()))
    with open(fname,'w',newline='') as csvfile:
#        topwriter = csv.DictWriter(csvfile,delimiter=',',
#                                    fieldnames=field_names)
        topwriter = csv.writer(csvfile,delimiter='\t')
        for key, value in tops_list.items():
            topwriter.writerow([key,value])
        #topwriter.writerows(list(zip(*[tops_list[key] for key in keys])))

def importer(file_name,tops_list,field_name):
    fname = file_name
    with open(fname,newline='') as csvfile:
        reader = csv.reader(csvfile,delimiter='\t')
        tops_list = dict(reader)
        print(tops_list)

def process_data():
    global data
    plt.style.use('ggplot')
    #Needs to be fixed so that parameters are not hard-coded in
    #Process the data and remove null values
    try:
        data = np.genfromtxt(fname,names=['MD','TVD','ROP','GR',
            'WOB','INC','CT_W','APRX','Flow','AZI','NBI'],skip_header=57)

    # Alternative file provided by mwd 
    except:
        data = np.genfromtxt(fname,names=['MD','ROP','TMP','TVD','GR'],skip_header=57)
    data['GR'][data['GR'] == -999.25] = np.nan
    data['ROP'][data['ROP'] == -999.25] = np.nan
    data['NBI'][data['NBI'] == -999.25] = np.nan
    data['WOB'][data['WOB'] == -999.25] = np.nan
    set_yscale()
#code below can be used to smooth data to a certain number of points
    #data['ROP'] = smooth(data['ROP'],50)
    #data['WOB'] = smooth(data['WOB'],10)

#adds the remarks - need a cleaner way to do this 

def add_markers(formation_info):
    print(formation_info)
    for key,value in formation_info.items():
        for i in value:
            name = key
            md = int(i)
            fm_name = name +' ' +  str(md) + ' MD'
            plt.plot([0,85],[md,md],color='black',linestyle='dashed')
            if key == 'Top' or key == 'Bottom':
                plt.text(2,md,fm_name,style='italic',size=7,
                        bbox={'facecolor':'orange','alpha':0.5,'pad':10})
            elif key == 'POOH':
                plt.text(2,md,fm_name,style='italic',size=7,
                        bbox={'facecolor':'red','alpha':0.5,'pad':10})
            else:
                plt.text(2,md,fm_name,style='italic',size=7,
                        bbox={'facecolor':'blue','alpha':0.5,'pad':10})

#Smooth curve function 
def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth


def gen_plot():
    global axes,fig
    fig,ax = plt.subplots(figsize=(12,80)) 
    plt.title(fname,y=1.25)

    #Add neccecary number of axis
    axes = [ax, ax.twiny(), ax.twiny(),ax.twiny()]
    add_markers(tops_list)
    fig.subplots_adjust(top=0.78)

def set_ax_pos():
#Decide axes location and color
    axes[-1].spines['top'].set_position(('axes',1.06))
    axes[-2].spines['top'].set_position(('axes',1.128))
    axes[-3].spines['top'].set_position(('axes',1.190))
    axes[0].spines['top'].set_color('blue')
    axes[1].spines['top'].set_color('black')
    axes[2].spines['top'].set_color('red')
    axes[3].spines['top'].set_color('green')
    axes[0].xaxis.set_ticks_position('top')
    axes[0].xaxis.set_label_position('top')
    axes[-1].set_frame_on(True)
    axes[-1].patch.set_visible(False)
    axes[0].set_label('X-axis')
    axes[0].set_ylim(min(data['MD']),max(data['MD']))


    #set tick locations and flip y axis in order to represent correct direction 
    plt.yticks(np.arange(round(min(data['MD']),-2),round(max(data['MD']),100),50))
    plt.gca().invert_yaxis()

#plots the actual data below 
#Labels and colours for drilling parameters
#Drilling parameters are likely to remain standard for this project
drill_params = ['ROP','NBI','GR','WOB','APRX']
#drill_params = ['ROP','GR']
#Colours as per the inteq screens on the rig for consistency 
colors = ['Blue','Black','Red','Green','Pink']

def set_limits(axes,colors,drill_params,option):

    #scales defined within scope of function >> need to be
    #stored somewhere so that the values are saved
    NBI_x = (70,100)
    ROP_x = (0,80)
    GR_x = (0,100)
    WOB_x = (0,10)
    APRX_x = (2000,4000)
    params = {'nbi':NBI_x,'rop':ROP_x,'gr':GR_x,'wob':WOB_x,'aprx':APRX_x}

    #sets scale from main loop
    if option == 'change':
        print(""">> Please enter the drilling param if you would 
        like to deviate from the original scale such as 
        ROP/WOB/INC/APRX/GR""")
        while True:
            check_param = input(">> Enter your drilling param or exit ")
            check_param = check_param.lower()
            check_param = check_param.strip()
            if check_param == 'exit':
                break
            for key,value in params.items():
                if check_param == key:
                    scale = input(">> input your required scale: ")
                    scale = scale.split(' ')
                    params[key] = int(scale[0]),int(scale[1])

    #sets scale if there are arguments
    if args.xscaler:
        params['rop'] = (int(args.xscaler.split(',')[0]),
        int(args.xscaler.split(',')[1]))

    if args.xscaleg:
        params['gr'] = (int(args.xscaleg.split(',')[0]),
        int(args.xscaleg.split(',')[1]))

    if args.xscalen:
        params['nbi'] = (int(args.xscalen.split(',')[0]),
        int(args.xscalen.split(',')[1]))

    if args.xscalea:
        params['aprx'] = (int(args.xscalea.split(',')[0]),
        int(args.xscalea.split(',')[1]))

    if args.xscalew:
        params['wob'] = (int(args.xscalew.split(',')[0]),
        int(args.xscalew.split(',')[1]))

    for ax, color,d in zip(axes,colors,drill_params):
        if d == 'NBI':
            ax.set_xlim(params['nbi'])
        if d == 'ROP':
            ax.set_xlim(params['rop'])
            ax.invert_xaxis()
        if d == 'GR':
            ax.set_xlim(params['gr'])
        if d == 'WOB':
            ax.set_xlim(params['wob'])
            ax.invert_xaxis()
        if d == 'APRX':
            ax.set_xlim(params['aprx'])
        ax.set_xlabel('%s' %d,color=color)
        ax.tick_params(axis='x',colors=color)
        ax.plot(data[d],data['MD'],color=color)



#remarks for annotations 
#this function was initially designed for creating persistent csv
#that will function as a database for each well
#initially will store an MD and obtain an XYZ coordinate from another survey
#file, this will allow us to keep all the important top information
#one file 




def set_yscale(upper=None,lower=None):
    #uses global y values in order to generate the needed 
    #plot information
    if upper == None and lower == None:
        y1 = round(max(data['MD']),100)-500
        y2 = round(max(data['MD']),100)
    else: 
        y1 = upper
        y2 = lower
    y1 = int(y1)
    y2 = int(y2)
    return(y1,y2)

def plot_graph(option=None):
    #all the steps needed to generate a plot
    process_data()
    gen_plot()
    set_ax_pos()
    set_limits(axes,colors,drill_params,option)
        
    if args.yscale:
        y1 = args.yscale.split(',')[0]
        y2 = args.yscale.split(',')[1]
        plt.ylim(set_yscale(y1,y2))
    else:
        try:
            plt.ylim(set_yscale(y1,y2))
        except:
            y1 = round(max(data['MD']),100)-500
            y2 = round(max(data['MD']),100)
            plt.ylim(set_yscale(y1,y2))
    plt.gca().invert_yaxis()
    #Set the gridlines below; need to design scheme for each 
    #drilling parameter
#    plt.grid(b=True, which ='both', color='0.65',linestyle='-')

#             --fname (as arg for las file to plot)
#             --anno *filename* (imports annotations)
#             --yscale 12000,12500 (sets y scale on init)
#             --xscaler 0,80 (sets rop scale on init)
#             --xscalea 2000,4000 (sets aprx scale on init)
#             --xscaleg 0,100 (sets gr scale on init)
#             --xscalew 0,10 (sets wob scale on init)
#             --xscalen 70,90 (sets nbi scale on init)
#
#             The other options avaiable will be 
#             during initialization of script: 
#             ran as tlas.py --fname *las
#             or --yscale 12000,12500 

def print_annotations(fm):
    print(tops_list)

def print_help():
    print("""
             Please enter one of the following options:
             either print - to create an svg of the file
             add md to add a new data point - note that 
             a note must also be added for the value to show
             so for example 12400 KC08 will annotate this 
             point
             

             during runtime:
             'pdf' to print to pdf
             'print' to print an svg 
             'add md' to add the new points (md for short) 
             'rm md' to remove the new points (rm for short) 
             'help' to see this message again 
             'show'/'plot' for an interactive plot
             'save' or 'export' will save the current tops
             'import' will bring back a set of interpreted
             'anno' will show the given points
             'changex' to change x scales before interactive mode 
             'pchx' to change x scales before printing an svg
             'pdfchx' to change x scales before printing a pdf 

             """)

adding_options = ['md','rm','rm md','add md']
annotation_options = ['annotations','annotate','anno']
change_yscale = ['changey','yscale','md scale','y']
interactive_options = ['interactive','i','inter','plot','show','changex']
help_options = ['help','h']
print_change_options = ['pchx','changex','print']
pdf_change_options = ['pdf','pdfchx']
exit_options = ['exit','quit','q','e']
save_options = ['save','export']
import_options = ['import']

try:
    load_name = args.anno
    importer(load_name,tops_list,field_names) 
except:
    print("No file was provided for previous interpretation")

print_help()

while True:
    option = input(">> ")
    option = option.strip()
    if option in exit_options: 
        break

    elif option in annotation_options: 
        print_annotations(tops_list)
    
    elif option in change_yscale:
        while True:
            y1 = input('Input the upper limit for your y axis: ')
            y2 = input('Input the lower limit for your y axis: ')
            set_yscale(y1,y2)
            break
            
    elif option in interactive_options:
        if option == 'changex':
            plot_graph(option='change')
        else:
            plot_graph()
        plt.show()

    elif option in help_options:
        print_help()

    elif option in print_change_options: 
        if option == 'pchx':
            plot_graph(option='change')
        else:
            plot_graph()
        plt.savefig(fname + '.svg')
    
    elif option in pdf_change_options:
        if option == 'pdfchx':
            plot_graph(option='change')
        else:
            plot_graph()
        pp = PdfPages(fname + '.pdf')
        #check if windows if true use below instead of other fname 
        #alternatively make fname cross platform (probably need to import sys.os)
        #pp = PdfPages('C:/Users/hp/Desktop/random_well.pdf')
        pp.savefig(fig)
        pp.close()

    elif option in adding_options:
        while True:
            fm_md = input(">> Please input MD and marker: ")
            if fm_md in exit_options:
                break
            try:
                md = fm_md.split(' ')[0]
                print(md + ' is your modified md')
                marker = fm_md.split(' ')[1]
                md = int(md)
                if option == 'rm md' or option == 'rm':
                    tops_list[marker].remove(md)
                else:
                    tops_list[marker].append(md)
                print("your modified list is \n")
                print(tops_list)
            except:
                print('Please enter a valid input\n')
                print('an error has been encountered')
    elif option in save_options:
        save_name = input(">> Please enter a file save name: ")
        writer(save_name,tops_list,field_names)
    elif option in import_options or 'import' in option:
        try:
            load_name = option.split(' ')[1]
            importer(load_name,tops_list,field_names) 
        except:
            load_name = input(">> Please enter a file load name: ")
            importer(load_name,tops_list,field_names) 
    else:
        print('This is not a valid option, please try again\n\n')
        print_help()
